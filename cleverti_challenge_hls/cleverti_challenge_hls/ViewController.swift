//
//  ViewController.swift
//  cleverti_challenge_hls
//
//  Created by Diogo Nunes on 04/09/2018.
//  Copyright © 2018 Diogo Nunes. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var dragAndDropView :DragAndDropView?
    var playerView : PlayerView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let width: CGFloat = 100.0 
        let circleFrame = CGRect(x: view.center.x - width / 2.0, y: view.center.y - width / 2.0, width: width, height: width)
        playerView = PlayerView(frame: circleFrame, fileUrl: "http://pubcache1.arkiva.de/test/hls_index.m3u8")
        playerView?.backgroundColor = UIColor.yellow
        dragAndDropView = DragAndDropView(frame: circleFrame, view: playerView!)
        view.addSubview(dragAndDropView!)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

