//
//  FileDownloader.swift
//  cleverti_challenge_hls
//
//  Created by Diogo Nunes on 05/09/2018.
//  Copyright © 2018 Diogo Nunes. All rights reserved.
//

import Foundation


class HLSFileDownloader {
    func downloadFileWithHighestQuality(urlString: String,
                                        progress: @escaping (Float) -> Void,
                                        completed: @escaping (String) -> Void)  {
        // let url = URL(string: urlString)!
        // let playlist = try! String.init(contentsOf: url, encoding: String.Encoding.utf8)
        // print("playlist\(playlist)")
        
        DispatchQueue.main.async {
            var seconds = 1;
            let increment = 0.1
            for currentProgress in stride(from: 0.0, to: 1.0, by: increment) {
                let deadlineTime = DispatchTime.now() + .seconds(seconds)
                seconds += 1
                
                DispatchQueue.main.asyncAfter(deadline: deadlineTime, execute: {
                    let normalizedProgress = Float(currentProgress + 0.1)
                    progress(normalizedProgress)
                    
                    if(normalizedProgress >= 1.0){
                        //add a small delay in to show the last progress update
                        DispatchQueue.main.async {
                            completed("Not implemented")
                        }
                    }
                })
            }
        }
    }
}
