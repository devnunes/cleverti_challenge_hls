//
//  PlayerPlayView.swift
//  cleverti_challenge_hls
//
//  Created by Diogo Nunes on 05/09/2018.
//  Copyright © 2018 Diogo Nunes. All rights reserved.
//

import Foundation
import UIKit
import AVKit

class PlayerPlayView : NSObject, PlayerStateView {
    
    let completed: ()->Void
    let audioPlayer: AVAudioPlayer
    
    let _view: UIView
    
    var view : UIView { return _view }
    
    init(filePath: String, completed:@escaping ()->Void) {
        
        self.completed = completed
        _view = UIView()
        _view.backgroundColor = UIColor.blue
        
        let soundFile = Bundle.main.path(forResource: filePath, ofType: nil)
        let url = URL(fileURLWithPath: soundFile!)
        audioPlayer = try! AVAudioPlayer(contentsOf: url as URL)
    }
    
    func setup() {
        audioPlayer.delegate = self
        audioPlayer.play()
        try! AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
    }
    
    func cleanup() {
        audioPlayer.stop()
        audioPlayer.delegate = nil
        view.removeFromSuperview()
    }
    
    func togglePause()  {
        if audioPlayer.isPlaying {
            audioPlayer.pause()
            _view.backgroundColor = UIColor.green
        } else {
            audioPlayer.play()
            _view.backgroundColor = UIColor.blue
        }
    }
}

extension PlayerPlayView : AVAudioPlayerDelegate {
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        if flag {
            self.completed()
        }
    }
}

