//
//  PlayerCompletedView.swift
//  cleverti_challenge_hls
//
//  Created by Diogo Nunes on 05/09/2018.
//  Copyright © 2018 Diogo Nunes. All rights reserved.
//

import Foundation
import UIKit

class PlayerCompletedView: PlayerStateView {
    
    let _view :UIView
    
    var view: UIView { return _view }
    
    init(onCompleted:@escaping () -> Void) {
        _view = UIView()
        _view.backgroundColor = UIColor.orange
        let spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        _view.addSubview(spinner)
        spinner.translatesAutoresizingMaskIntoConstraints = false
        let centerXConstraint = NSLayoutConstraint(item: spinner, attribute: .centerX, relatedBy: .equal, toItem: _view, attribute: .centerX, multiplier: 1, constant: 0)
        let centerYConstraint = NSLayoutConstraint(item: spinner, attribute: .centerY, relatedBy: .equal, toItem: _view, attribute: .centerY, multiplier: 1, constant: 0)
        NSLayoutConstraint.activate([centerXConstraint, centerYConstraint])
        spinner.startAnimating()
        
        //simulate file deletion
        let deadlineTime = DispatchTime.now() + .seconds(2)
        DispatchQueue.main.asyncAfter(deadline: deadlineTime, execute: {
            onCompleted()
        })
    }
    
    func setup() {
    }
    
    func cleanup() {
        view.removeFromSuperview()
    }
}
