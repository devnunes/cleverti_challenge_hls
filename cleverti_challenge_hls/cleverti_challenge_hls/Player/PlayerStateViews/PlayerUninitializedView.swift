
//
//  PlayerLoadingView.swift
//  cleverti_challenge_hls
//
//  Created by Diogo Nunes on 04/09/2018.
//  Copyright © 2018 Diogo Nunes. All rights reserved.
//

import Foundation
import UIKit

class PlayerUninitializedView : PlayerStateView   {
    let _view: UIView
    
    var view: UIView { return _view }
    
    init() {
        _view = UIImageView(image: UIImage(named: "play_icon"))
    }

    func setup() {
        //No setup needed
    }
    
    func cleanup() {
        view.removeFromSuperview()
    }
}
